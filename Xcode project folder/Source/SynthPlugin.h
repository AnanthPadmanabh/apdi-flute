//
//  SynthPlugin.h
//  TestSynthAU
//
//  Software Synthesizer template project for UWE AMT/CMT students.
//

#ifndef __SynthPlugin_h__
#define __SynthPlugin_h__

#include "PluginProcessor.h"
#include "SynthExtra.h"

enum TIMBRE
{
    MELODY,
    HARMONY,
    BASS
};

static TIMBRE getTimbre(const int pitch, const float velocity) {
    if(velocity == 1.0){    // is melody or bass (lead/arp)
        return pitch >= 66 ? MELODY : BASS;
    }else{                  // is harmony / chords
        return HARMONY;
    }
}

//===================================================================================
/** An example STK-voice, based on a sine wave generator                           */
class MyVoice : public Voice
{
public:
    void onStartNote (const int pitch, const float velocity);
    bool onStopNote ();
    
//    void onPitchWheel (const int value);
//    void onControlChange (const int controller, const int value);
    
    bool process (float** outputBuffer, int numChannels, int numSamples);
    
private:
    Sine tremoloVariaton;
    
    Envelope breathPressure;
    
    Envelope ampEnv;
    
    Noise breathNoise;
    
    LPF noiseFilt;
    
    Delay fwdDelResonator;
    
    float fresoDelTime;
    
    LPF resFilt;
    
    HPF finalFilt;
    
    
    float noteFrequency;
    float fLevel;
};

class MySynth : public Synth
{
public:
    MySynth() : Synth() {
        initialise();
    }
    
    void initialise();
    void postProcess(float** outputBuffer, int numChannels, int numSamples);

private:
    // Insert synthesizer variables here
};

#endif
