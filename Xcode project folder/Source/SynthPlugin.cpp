//
//  SynthPlugin.cpp
//  TestSynthAU
//
//  Software Synthesizer template project for UWE AMT/CMT students.
//

#include "SynthPlugin.h"

////////////////////////////////////////////////////////////////////////////
// SYNTH - represents the whole synthesiser
////////////////////////////////////////////////////////////////////////////

// Called to create the synthesizer (use to point JUCE to your synthesizer)
Synth* JUCE_CALLTYPE createSynth() {
    return new MySynth();
}

// Called when the synthesiser is first created
void MySynth::initialise()
{
    // Initialise synthesiser variables here
}

// Used to apply any additional audio processing to the synthesisers' combined output
// (when called, outputBuffer contains all the voices' audio)
void MySynth::postProcess(float** outputBuffer, int numChannels, int numSamples)
{
    // Use to add global effects, etc.
    float fDry0, fDry1;
    float* pfOutBuffer0 = outputBuffer[0];
    float* pfOutBuffer1 = outputBuffer[1];
    
    while(numSamples--)
    {
        fDry0 = *pfOutBuffer0;
        fDry1 = *pfOutBuffer1;
        
        // Add your global effect processing here
        
        *pfOutBuffer0++ = fDry0;
        *pfOutBuffer1++ = fDry1;
    }
}

////////////////////////////////////////////////////////////////////////////
// VOICE - represents a single note in the synthesiser
////////////////////////////////////////////////////////////////////////////

// Called to create the voice (use to point JUCE to your voice)
Voice* JUCE_CALLTYPE createVoice() {
    return new MyVoice();
}

// Triggered when a note is started (use to initialise / prepare note)
void MyVoice::onStartNote (const int pitch, const float velocity)
{
    noteFrequency = MidiMessage::getMidiNoteInHertz (pitch);
    
    
    // Breath pressure input Envelope
    breathPressure.set(Envelope::Points(0.0,0.0)(0.064,0.613)(0.138,1.0)(0.665,1.0)(0.729,0.613)(0.787,0.0));
    // Final output envelope
    ampEnv.set(Envelope::Points(0.0,0.0)(0.064,0.70)(0.138,1.0)(0.365,0.6)(0.6,0.2)(0.787,0.0));
    
    // Envelope Sustain
    breathPressure.setLoop(2,2);
    ampEnv.setLoop(3, 3);
    
    // Initialise Delay
    fresoDelTime = (( 1 / (2 * noteFrequency) ) * getSampleRate() ) - 5.54;
    fwdDelResonator.setMaximumDelay(getSampleRate());
    fwdDelResonator.clear();
    fwdDelResonator.setDelay(fresoDelTime);

    
    fLevel = velocity;
}

// Triggered when a note is stopped (return false to keep the note alive)
bool MyVoice::onStopNote (){
    return true;
}

// Called to render the note's next buffer of audio (generates the sound)
// (return false to terminate the note)
bool MyVoice::process (float** outputBuffer, int numChannels, int numSamples)
{
    float fMix, fMixMult1 = 0.0, fNoise = 0.0, fMixAdd1 = 0.0, fMixAdd2= 0.0;
    float* pfOutBuffer0 = outputBuffer[0];
    float* pfOutBuffer1 = outputBuffer[1];
    
    float f_t = getParameter(6) * 3.0 + 4.0;  // Tremolo Rate
    float a_t = getParameter(5);              // Tremolo Depth
    float a_p = 0.015;                        // Pressure scaling
    float f_n = 4000.0;                       // Noise Filter cutoff
    float a_n = 0.075;                        // Noise scaling
    
    float a_m1 = getParameter(9) * 0.3844 + 0.5344; // Scaling value
    float a_m2 = getParameter(9) * 0.1845 + 0.8155; // Offsetting value
    float f_r = 0.195 * 11950.0 + 50.0;       // Resonator cutoff = 2380.25 Hz
    float a_r = getParameter(9) * 0.616 + 1.184;        // Resonant scaling
    
    
    //Set filters
    noiseFilt.setCutoff(f_n);
    resFilt.setCutoff(f_r);
    finalFilt.setCutoff(100);
    
    //Set freq
    tremoloVariaton.setFrequency(f_t);
    
    while(numSamples--)
    {
        
        
        //BREATH
        
        
        //Breath Pressure
        
        fMixMult1 = tremoloVariaton.tick() * a_t + 1.0 ;
        
        fMix = breathPressure.tick() * fMixMult1;
        
        fNoise = noiseFilt.tick( breathNoise.tick() ) * fMix ;
        
        fMix = fMix * a_p + fNoise * a_n;
        
        //Feedback
        fMix += fMixAdd2;
        
        //Inverse Multiplication
        fMixAdd1 = fMix;
        fMixAdd1 *= -1;
        
        
        //MOUTHPIECE
        
        
        //Scale and Offset
        fMix = fMix * a_m1 + a_m2;
        
        //Hard Clipping
        if(fMix > 1.0)
        {
            fMix = 1.0;
        }
        else if (fMix < -1.0)
        {
            fMix = -1.0;
        }
        
        //Shaping function
        fMix = pow(fMix,3);
        
        //Multiplication
        fMix *= fMixAdd1;
        
        
        //TUBE RESONATOR
        
        
        //Delay
        fMix = fwdDelResonator.tick(fMix);    // Delay
        //Filter
        fMix = resFilt.tick(fMix);            //Resonant Filter
        fMix *= a_r;
        
        //Feedback Addition
        fMixAdd2 = fMix;
        
        //Final Out
        fMix = finalFilt.tick(fMix);          //Filter to remove low rumble and D.C Offset.
        fMix *= fLevel * ampEnv.tick() * 0.6; //Applying final envelope and scaling output.
        
        *pfOutBuffer0++ = fMix;
        *pfOutBuffer1++ = fMix;
    }
    
    return ampEnv.getStage() != Envelope::STAGE::ENV_OFF;
}
